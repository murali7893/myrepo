package com.imt.java;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class StringtoUrlTransformer extends AbstractMessageTransformer{
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		// TODO Auto-generated method stub
		   String url=message.getInvocationProperty("url");
		   URI urlObj=null;
		   try {
			   urlObj=new URI(url);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return urlObj;
	}
	
}

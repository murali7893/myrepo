package com.imt.java;

import java.util.ArrayList;
import java.util.List;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class HikeSalaryComponent implements Callable
{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		Employees employees=(Employees)eventContext.getMessage().getPayload();
		List<Employee> listEmp=employees.getListOfEmployee();
		List<Employee> updateList=new ArrayList<Employee>();
		Employees updateEmployees=new Employees();
		for(Employee tempObj:listEmp)
		{
			double updateSalary=tempObj.getEmpSalary()+tempObj.getEmpSalary()*10/100;
			tempObj.setEmpSalary(updateSalary);
			updateList.add(tempObj);
		}
		updateEmployees.setListOfEmployee(updateList);
		
		return updateEmployees;
	}
	
}

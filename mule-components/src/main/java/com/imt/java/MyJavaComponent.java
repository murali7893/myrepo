package com.imt.java;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class MyJavaComponent implements Callable{

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		
	Employee empObj	=(Employee)eventContext.getMessage().getPayload();
	double updateSalry=empObj.getEmpSalary()+empObj.getEmpSalary()*10/100;
	empObj.setEmpSalary(updateSalry);
	return empObj;
	}

}

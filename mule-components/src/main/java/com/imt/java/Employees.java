package com.imt.java;

import java.util.List;

public class Employees {

	List<Employee> listOfEmployee;

	public List<Employee> getListOfEmployee() {
		return listOfEmployee;
	}

	public void setListOfEmployee(List<Employee> listOfEmployee) {
		this.listOfEmployee = listOfEmployee;
	}
	
}
